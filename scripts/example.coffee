# Description:
#   Example scripts for you to examine and try out.
#
# Notes:
#   They are commented out by default, because most of them are pretty silly and
#   wouldn't be useful and amusing enough for day to day huboting.
#   Uncomment the ones you want to try and experiment with.
#
#   These are from the scripting documentation: https://github.com/github/hubot/blob/master/docs/scripting.md
pass = undefined

module.exports = (robot) ->
  
   lulz = ['lol', 'rofl', 'lmao']
   
   acketz = [
     'Cool Story Bro'
     'Si'
     '42'
     'Things and Stuff'
     'Cuz Reasons'
     'Nope'
     'Eleventy.0 Per Cent Chance'
     'Eleventy % chance', 'Yup'
     'Tiger Balls?'
     'Tokyo'
     'Nope, not even a cool story'
     "not sure you meet 'bro' status anymore"
     'Bro status officially revoked'
     'What do I look like? A Magic 8 Ball? Figure it out yourself.'
     'I want to be shocked, but the only emotion available for response is boredom. -_-'
     'What a fantastic story! Can I go home now?'
     "I don't get paid enough to come up with a witty response to that."
     'Are you cereal with me right now?'
     'Woah! A real live herpaderp'
     "that's exciting!"
     'Heyyy! Bend over!'
     'WTF Mate!'
   ]
   
   robot.hear /twss/i, (res) ->
     room = res.message.room
	 
     if room is "general" or room is "important-info" or room is "safe-handfruit-test"
       pass
     else
       res.send "That's what she said"
   
   robot.hear /Magic(.*)Acket(.*)Ball/i, (res) ->
     randacket = res.random acketz
     room = res.message.room
     
     if room is "general" or room is "important-info" or room is "safe-handfruit-test"
       pass
     else
       res.send "Magic Acket Ball Sez: #{randacket}"
   
   robot.hear /hand([^_]*?)fruit/i, (res) ->
     room = res.message.room
     usr = res.message.user.real_name
     
     if room is "general" or room is "important-info" or room is "safe-handfruit-test"
       res.send "Please, no hand fruit here.  Use #handfruitdiscussion instead"
     else
       res.send "No way, #{usr}!  No Hand Fruit for you!"
  
   robot.hear /room info/i, (res) ->
     res.send "#{res.message.user.name} wants to know what channel this is."
     res.send "This room is: #{res.message.room}"

   robot.respond /What(.*)port(.*)/i, (res) ->
     if robot.auth.hasRole(msg.envelope.user, 'super_user')
       port = process.env.EXPRESS_PORT
       unless port?
         res.send "Express port not set." 
         port = process.env.PORT

       res.send "Currently operating on port #{port}"
     else
       res.send "Unauthorized"
     
   robot.respond /random/i, (res) ->
     res.send "#{Math.random()}"
     
   robot.respond /Who am i/i, (res) ->
     res.send "You are #{res.message.user.real_name}"
     res.send "Your userid is #{res.message.user.name}"
  
   answer = process.env.HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING
  
   robot.respond /what is the answer to the ultimate question of life/, (res) ->
     unless answer?
       res.send "Missing HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING in environment: please set and try again"
       return
     res.send "#{answer}, but what is the question?"
  
   robot.hear /you (.*) slow/, (res) ->
     setTimeout () ->
       res.send "Who you calling 'slow'?"
     , Math.random() * 60 * 1000
  
  # annoyIntervalId = null
  #
  # robot.respond /annoy me/, (res) ->
  #   if annoyIntervalId
  #     res.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #     return
  #
  #   res.send "Hey, want to hear the most annoying sound in the world?"
  #   annoyIntervalId = setInterval () ->
  #     res.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #   , 1000
  #
  # robot.respond /unannoy me/, (res) ->
  #   if annoyIntervalId
  #     res.send "GUYS, GUYS, GUYS!"
  #     clearInterval(annoyIntervalId)
  #     annoyIntervalId = null
  #   else
  #     res.send "Not annoying you right now, am I?"
  #
  #
   robot.router.post '/hubot/chatsecrets/:room', (req, res) ->
     room   = req.params.room
     data   = JSON.parse req.body.payload
     secret = data.secret
  
     robot.messageRoom room, "I have a secret: #{secret}"
  
     res.send 'OK'
     res.writeHead 200, {'Content-Type': 'text/plain'}
     res.end 'Thanks\n'
   
   robot.error (err, res) ->
     robot.logger.error "DOES NOT COMPUTE"
  
     if res?
       res.reply "DOES NOT COMPUTE"
       
